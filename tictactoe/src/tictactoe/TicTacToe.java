package tictactoe;

/**
 * The two-player game of TicTacToe.
 *
 * The goal is to get three cells in a row.
 *
 * @author Mark Utting
 */
public class TicTacToe {

	/**
	 * Checks to see if a cell is already taken.
	 * @param row must be 0..2
	 * @param col must be 0..2
	 * @return true if cell (row,col) is already full.
	 */
	public boolean isTaken(int row, int col) {
		return false;
	}

	/**
	 * Get a given cell.
	 * 
	 * @return the contents of a given cell (0, 1 or 2).
	 */
	public int getCell(int row, int col) {
		return 0;
	}

	/**
	 * Tells which player has won.
	 * If there is no winner yet, the result will be zero.
	 * If the game is over, but is a draw, the result is 3 (=2+1).
	 *
	 * @return 1 or 2, or 3 (draw), or 0 (no winner yet)
	 */
	public int winner() {
		return 0;
	}

	/**
	 * Player 1 makes a move.
	 * 
	 * The result is true if the requested move was valid,
	 * and in that case, the cell (row,col) will be filled.
	 * A valid move requires:
	 * <ul>
	 *   <li>the cell (row,col) was empty before the move;</li>
	 *   <li>it was this player's turn to play (either player can start);</li>
	 *   <li>the game was not already won.</li>
	 * </ul>
	 *
	 * The result is false if the move was invalid,
	 * and in that case the board will be left unchanged.
	 *
	 * @param row 0..2
	 * @param col 0..2
	 * @return true iff this was a valid move 
	 */
	public boolean move1(int row, int col) {
		return false;
	}
	
	/**
	 * Player 2 makes a move.
	 * 
	 * The result is true if the requested move was valid,
	 * and in that case, the cell (row,col) will be filled.
	 * A valid move requires:
	 * <ul>
	 *   <li>the cell (row,col) was empty before the move;</li>
	 *   <li>it was this player's turn to play (either player can start);</li>
	 *   <li>the game was not already won.</li>
	 * </ul>
	 *
	 * The result is false if the move was invalid,
	 * and in that case the board will be left unchanged.
	 *
	 * @param row 0..2
	 * @param col 0..2
	 * @return true iff this was a valid move 
	 */
	public boolean move2(int row, int col) {
		return false;
	}

	/**
	 * Returns three strings that display the current state of the game.
	 * Example:
	 * <pre>
	 *  row[0]: " 2 . 1 "
	 *  row[1]: " 2 1 . "
	 *  row[2]: " 1 . . "
	 * </pre>
	 * @return three strings, one per row.
	 */
	public String[] display() {
		String lines[] = new String[3];
		for (int row = 0; row < 3; row++) {
			// A StringBuilder is like a String, but with update methods.
			StringBuilder str = new StringBuilder();
			for (int col = 0; col < 3; col++) {
				int cell = getCell(row, col);
				if (cell == 0) {
					str.append(" .");
				} else {
					str.append(" " + cell);
				}
			}
			lines[row] = str.toString();
		}
		return lines;
	}

}
